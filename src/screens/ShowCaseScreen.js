import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class ShowCaseScreen extends Component{
  render() {
    return (
      <View style={styles.container}>
        <Text>ShowCaseScreen</Text>
      </View>
    );
  }
}

export default ShowCaseScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF'}
    });