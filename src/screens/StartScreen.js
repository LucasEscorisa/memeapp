import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Dimensions,
  Animated,
  ScrollView,
  Image,
  StatusBar
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons'

import MList from '../components/MList';

const optionsIcon = Platform.OS === 'ios' ? "ios-options" : "md-options"

let { width, height } = Dimensions.get('window');

class StartScreen extends Component {

  constructor(props) {

    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
    };

  }

  render() {

    const HEADER_MAX_HEIGHT = height / 2;
    const HEADER_MIN_HEIGHT = height / 6;
    const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 1.8],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp'
    });

    const lineOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 1.8],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });

    return (
      <SafeAreaView style={styles.container}>

        <StatusBar 
          backgroundColor="transparent"
          translucent={true}
          barStyle="light-content" />

        <Animated.View
          style=
          {[styles.header,
          {
            height: HEADER_MAX_HEIGHT,
            transform: [{ translateY: headerHeight }]
          }]}>

          <LinearGradient

            colors={['#53009A', '#8900FF']}
            style={styles.HeaderGradient}

          />
        </Animated.View>

        <View style={styles.headerbar}>
          <View style={styles.headercontainer}>
            <Text style={styles.headertitle}>Início</Text>
            <Icon name={optionsIcon} color={"white"} size={32}/>
          </View>
          <Animated.View 
            style={{ 
              opacity: lineOpacity, 
              width: '100%', 
              borderTopColor: '#7F2AC7', 
              marginTop: 10, 
              borderTopWidth: 1 }} />
        </View>

        <Animated.ScrollView 
		    	style={{ paddingTop: 20, marginTop: 5 }}
		    	scrollsToTop
		    	scrollEventThrottle={5}
			    onScroll={Animated.event(
			      [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}], { useNativeDriver: true, }
			    )}>

          <MList
            title='Novidades'
            titleColor='white'
            categoryID='7'
            cardSize='regular'
          />

          <MList 
    				title='Destaques'
    				categoryID='8'
    				cardSize='small'
    			/>


	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
	    		<Text>Início</Text>
          
          </Animated.ScrollView>

        
      </SafeAreaView>
    );
  }
}

export default StartScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  header: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0
  },
  HeaderGradient: {
    height: '100%',
  },
  headerbar: {
    paddingHorizontal: 20
  },
  headercontainer: {
    marginTop: 25,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headertitle: {
    flex: 8,
    textAlign: 'left',
    color: 'white',
    fontSize: 34,
    fontWeight: '700'
  }
});

