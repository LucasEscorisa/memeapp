import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class FavoritesScreen extends Component{
  render() {
    return (
      <View style={styles.container}>
        <Text>FavoritesScreen</Text>
      </View>
    );
  }
}

export default FavoritesScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF'}
    });