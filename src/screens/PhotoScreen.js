import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class PhotoScreen extends Component{
  render() {
    return (
      <View style={styles.container}>
        <Text>PhotoScreen</Text>
      </View>
    );
  }
}

export default PhotoScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF'}
    });