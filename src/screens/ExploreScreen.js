import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class ExploreScreen extends Component{
  render() {
    return (
      <View style={styles.container}>
        <Text>ExploreScreen</Text>
      </View>
    );
  }
}

export default ExploreScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'}
  });