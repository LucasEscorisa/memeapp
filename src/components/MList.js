import React, { Component } from 'react';
import { 
    Image,
    Dimensions, 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    SafeAreaView, 
    ActivityIndicator, 
    FlatList, 
    TouchableOpacity } from 'react-native';

import { withNavigation } from 'react-navigation';

let {width, height} = Dimensions.get('window');

class MList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,

            cardWidth: null,
            cardHeight: null,
            fontTop: null,
            fontSize: null
        }
    }

    componentDidMount() {

        if (this.props.cardSize == 'regular') {

            this.setState({
                cardWidth: width / 1.4,
                cardHeight: height / 3,
                fontTop: 14,
                fontSize: 24
            });
        }

        if (this.props.cardSize == 'small') {

            this.setState({
                cardWidth: width / 2.1,
                cardHeight: height / 3,
                fontTop: 12,
                fontSize: 18,
            });


        }

        if (this.props.searchTerms) {

            urlAPI = 'http://treeep.com/memeapp/wp-json/wp/v2/posts/?search=' + this.props.searchTerms + '&_embed';

        } else {

            urlAPI = 'http://treeep.com/memeapp/wp-json/wp/v2/posts/?categories=' + this.props.categoryID + '&_embed';

        }

        return fetch(urlAPI)
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({
                    isLoading: false,
                    dataSource: responseJson
                }, function () {

                });

            })

            .catch((error) => {
                console.error(error);
            });

    }

    render() {

        if (this.state.isLoading) {
            return (
                <View style={{ height: this.state.cardHeight+20, flex: 1 }}>

                    <Text 
                        style=
                            {[styles.show, 
                            { color: this.props.titleColor }]}>
                            
                            {this.props.title}
                    </Text>

                    <ActivityIndicator style={{ flex: 1 }} />

                </View>
            )
        }
        else {
            return (
                <FlatList
                    data={this.state.dataSource}
                    snapToInterval={this.state.cardWidth+10}
                    decelerationRate='fast'
                    snapToAlignment='start'
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={styles.list}
                    contentContainerStyle={{ paddingRight: 20 }}
                    keyExtractor={(item) => item.slug}
                    renderItem={({ item }) => {
                        const id = item.id;
                        let titulo = item.title.rendered;
                        let thumb = item._embedded['wp:featuredmedia']['0'].source_url;

                        return (

                            <TouchableOpacity 
                                activeOpacity={0.7} 
                                onPress={() => {this.props.navigation.navigate('ShowcaseScreen', 
                                { id: id, nome: titulo, imagem: thumb })}}>
                                <View style={styles.card}>
                                    <View
                                        style=
                                        {[styles.image,
                                        {
                                            width: this.state.cardWidth,
                                            height: this.state.cardHeight
                                        }]}>
                                        <Image
                                            indicator={ActivityIndicator}
                                            source={{ uri: thumb }}
                                            imageStyle={{ borderRadius: 4 }}
                                            style={{ width: '100%', height: '100%' }} />
                                    </View>
                                    <Text
                                        style=
                                        {{
                                            width: this.state.cardWidth,
                                            fontWeight: '600',
                                            color: this.props.textColor,
                                            fontSize: this.state.fontSize,
                                            marginTop: this.state.fontTop

                                        }}>

                                        {titulo}

                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                />


            );
        }

    }
}

MList.defaultProps = {

    title: 'Mais Vistos',
    titleColor: '#5800A4',
    textColor: '#5800A4',
    categoryID: '',
    cardSize: 'regular',
    searchTerms: '',

};

export default withNavigation(MList);

const styles = StyleSheet.create({
    show: {
        paddingLeft: 20,
        fontSize: 26,
        fontWeight: '700',
        marginBottom: 20,
        overflow: 'visible'
    },
    list: {
        paddingLeft: 20,
        paddingRight: 100,
        marginBottom: 30,
        overflow: 'visible'
    },
    card: {
        marginRight: 10,
    },
    image: {

    }
});