/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import LinearGradient from 'react-native-linear-gradient';

import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Explore from './src/screens/ExploreScreen';
import Favorites from './src/screens/FavoritesScreen';
import Photo from './src/screens/PhotoScreen';
import ShowCase from './src/screens/ShowCaseScreen';
import Start from './src/screens/StartScreen';

{/* Váriaveis úteis */}
let bottomTabIconSize = 26;

{/* Navegação em pilha pela aba Início + estilos e parâmetros */}
const StartNavigator = createStackNavigator({
  
  StartScreen: Start,
  ShowcaseScreen: ShowCase,
  PhotoScreen: Photo,

  },{
    headerMode: 'none',
});
  
StartNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 1) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

const FavoritesNavigator = createStackNavigator({
  FavoritesScreen: Favorites,
  PhotoScreen: Photo,
}, {
  headerMode: 'none',
});

FavoritesNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

{/* Navegação BottomTabs */}
const AppNavigator = createMaterialBottomTabNavigator({
  Start: {
    screen: StartNavigator,
    navigationOptions: {
      tabBarIcon: ({tintColor}) => (
        <Icon name="md-albums" color={tintColor} size={bottomTabIconSize} />
      )
    } 
  },
  Explore: {
    screen: Explore,
    navigationOptions: {
      tabBarIcon: ({tintColor}) => (
        <AntDesignIcon name="search1" color={tintColor} size={bottomTabIconSize} />
      )
    } 
    
  },
  Favorites:{
    screen: Favorites,
    navigationOptions: {
      tabBarIcon: ({tintColor}) => (
        <AntDesignIcon name="staro" color={tintColor} size={bottomTabIconSize} />
      )
    } 
  }
},
{
  activeTintColor: "blue",
  barStyle:{backgroundColor: "white"},
  labeled: false
  
})

export default createAppContainer(AppNavigator);